//
//  JxtaPic.m
//  Juxt-a-pose
//
//  Created by Christopher on 11/19/13.
//  Copyright (c) 2013 We Are Station. All rights reserved.
//

#import "JxtaPic.h"
#import "JPic.h"

@implementation JxtaPic;

@synthesize pictures;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithJPic:(JPic*)jpic
{
    self = [super initWithImage:jpic.image];
    if (self) {
        // Initialization code
    }
    return self;
}


-(void)addImage:(JPic*)jpic{
    [pictures addObject:jpic];
}


-(UIImage *)exportImage{
    UIImage *exported = [[UIImage alloc]init];
    return exported;

}


-(void)removeImage{}


-(void)editImage{}


-(UIImage *)showpreview{
UIImage *showpreview = [[UIImage alloc]init];
    return showpreview;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
