//
//  JxtaPic.h
//  Juxt-a-pose
//
//  Created by Christopher on 11/19/13.
//  Copyright (c) 2013 We Are Station. All rights reserved.
//

#import <UIKit/UIKit.h>
@class JPic;

@interface JxtaPic : UIImageView{
    
   
    
}


@property (nonatomic, strong) NSMutableArray *pictures;

-(void)addImage:(JPic*)jpic;
-(UIImage*)exportImage;
-(void)removeImage;
-(void)editImage;
-(UIImage*)showpreview;


@end
