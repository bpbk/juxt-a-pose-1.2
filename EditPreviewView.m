//
//  EditPreviewView.m
//  Juxt-a-pose
//
//  Created by Brandon Phillips on 11/27/13.
//  Copyright (c) 2013 We Are Station. All rights reserved.
//

#import "EditPreviewView.h"

@interface EditPreviewView ()

@end

@implementation EditPreviewView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
