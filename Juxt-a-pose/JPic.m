//
//  JPic.m
//  Juxt-a-pose
//
//  Created by Christopher on 11/19/13.
//  Copyright (c) 2013 We Are Station. All rights reserved.
//

#import "JPic.h"



@implementation JPic

@synthesize image,opacity,position,size;

- (id)initWithOptions:(UIImage*)images
                   op:(CGFloat)opacitys
                  pos:(CGPoint)positions
                  siz:(CGPoint)sizes
{
    self = [super init];
    if (self) {
        // Initialization code
        opacity = opacitys;
        image = images;
        position = positions;
        size = sizes;
    }
    return self;
}




@end
