//
//  SharePhoto.m
//  Juxt-a-pose
//
//  Created by Brandon Phillips on 6/6/13.
//  Copyright (c) 2013 We Are Station. All rights reserved.
//

#import "SharePhoto.h"
#import "AllPhotos.h"
#import "AppDelegate.h"
#import "DMActivityInstagram.h"

@interface SharePhoto ()

@end

@implementation SharePhoto

- (IBAction)goBack{
    shareIt.hidden = TRUE;
    blendedImage = NULL;
    [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"not_first_run"];
    [[NSUserDefaults standardUserDefaults] setObject:@"firstCancel" forKey:@"base_photo"];
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)removePhoto{
    NSString *urlString = [[NSString alloc] initWithFormat:@"file://%@", self.blendedImageURL];
    NSURL *imageUrl = [[NSURL alloc] initWithString: urlString];
    [[NSFileManager defaultManager] removeItemAtURL:imageUrl error:NULL];
    
}


- (IBAction)share:(id)sender {
    shareIt.hidden = TRUE;
    [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"not_first_run"];
    DMActivityInstagram *instagramActivity = [[DMActivityInstagram alloc] init];
    
    NSString *shareText = @"Posted from Juxt-a-pose #JXTP";
    //NSURL *shareURL = [NSURL URLWithString:@"http://catpaint.info"];
    
    NSArray *activityItems = @[[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:_blendedImageURL]]], shareText];
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:@[instagramActivity]];
    [self presentViewController:activityController animated:YES completion:nil];
}


- (IBAction)takeNew{
    shareIt.hidden = TRUE;
    blendedImage = NULL;
    img = NULL;
    [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"not_first_run"];
    [[NSUserDefaults standardUserDefaults] setObject:@"first" forKey:@"base_photo"];
     [self dismissViewControllerAnimated:NO completion:NULL];
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)loadView{
    if([[UIScreen mainScreen] bounds].size.height == 568)
    {
        // iPhone 5
        self.view = [[NSBundle mainBundle] loadNibNamed:@"SharePhoto" owner:self options:nil][0];
    }
    else
    {
        self.view = [[NSBundle mainBundle] loadNibNamed:@"SharePhotoFour" owner:self options:nil][0];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    shareIt.hidden = TRUE;
    NSString *flag = [[NSUserDefaults standardUserDefaults] stringForKey:@"not_first_run"];
    if(!flag){
        shareIt.hidden = FALSE;
        startNew.hidden = TRUE;
    }else{
        shareIt.hidden = TRUE;
        startNew.hidden = FALSE;
    }
    
    
    self.finalImageView.image =[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:_blendedImageURL]]];
    
    //NSString *flag = [[NSUserDefaults standardUserDefaults] stringForKey:@"not_first_run"];
    if (!flag) {
                //tutorialOverlay.hidden = FALSE;
    }else{
                //tutorialOverlay.hidden = TRUE;
    }
    NSManagedObject *photoModel = [NSEntityDescription insertNewObjectForEntityForName:@"AFPhotoModel" inManagedObjectContext:managedObjectContext];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            [photoModel setValue:_blendedImageURL forKey:@"photoImageData"];
            [photoModel setValue:_blendedImageName forKey:@"photoName"];
            
        });
    });
    // Do any additional setup after loading the view from its nib.
}

- (UIDocumentInteractionController *) setupControllerWithURL: (NSURL*) fileURL usingDelegate: (id <UIDocumentInteractionControllerDelegate>) interactionDelegate
{
    
    
    UIDocumentInteractionController *interactionController = [UIDocumentInteractionController interactionControllerWithURL:fileURL];
    interactionController.delegate = self;
    
    return interactionController;
}

- (void)documentInteractionControllerWillPresentOpenInMenu:(UIDocumentInteractionController *)controller
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
