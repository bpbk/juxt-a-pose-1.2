//
//  ViewController.m
//  Juxt-a-pose
//
//  Created by Brandon Phillips on 5/25/13.
//  Copyright (c) 2013 We Are Station. All rights reserved.
//

#import "ViewController.h"
#import "SharePhoto.h"
#import "AppDelegate.h"
#import "AllPhotos.h"
#import "PhotoLibrary.h"
#import "JxtaPic.h"
#import "JPic.h"


@interface ViewController ()

@end

@implementation ViewController
//@synthesize sliderValue;


-(IBAction)seeAll{
    blendedImage = nil;
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(IBAction)finished{
     [self finishedPressed];
}

- (IBAction)cameraFlip{
    if( [UIImagePickerController isCameraDeviceAvailable: UIImagePickerControllerCameraDeviceFront]){
        if(picker.cameraDevice == UIImagePickerControllerCameraDeviceRear){
            
            picker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
            cameraRotation = 1;
            
        }else{
            picker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
            cameraRotation = 0;
        }
    }
}

- (IBAction)FlashToggle{
   

    UIImagePickerControllerCameraFlashMode flashSet = picker.cameraFlashMode;
    switch(flashSet) {
    
        case UIImagePickerControllerCameraFlashModeOn:
            [picker setCameraFlashMode:UIImagePickerControllerCameraFlashModeAuto];
            [[NSUserDefaults standardUserDefaults] setObject:@"flashAuto" forKey:@"flash_mode"];
            [toggleFlashMode setBackgroundImage:[UIImage imageNamed:@"flashAuto.png"] forState:UIControlStateNormal];
            break;
        case UIImagePickerControllerCameraFlashModeAuto:
            [picker setCameraFlashMode:UIImagePickerControllerCameraFlashModeOff];
            [[NSUserDefaults standardUserDefaults] setObject:@"flashOff" forKey:@"flash_mode"];
            [toggleFlashMode setBackgroundImage:[UIImage imageNamed:@"flashOff.png"] forState:UIControlStateNormal];
            break;
        case UIImagePickerControllerCameraFlashModeOff:
            [picker setCameraFlashMode:UIImagePickerControllerCameraFlashModeOn];
            [[NSUserDefaults standardUserDefaults] setObject:@"flashOn" forKey:@"flash_mode"];
            [toggleFlashMode setBackgroundImage:[UIImage imageNamed:@"flashOn.png"] forState:UIControlStateNormal];
            break;
    }
}

- (void) finishedPressed{

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    
    NSDate *date = [[NSDate alloc] init];
    
    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    pngData = UIImageJPEGRepresentation(blendedImage, 1.0);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
    NSString *fileName = [NSString stringWithFormat:@"image_%@.jpg", formattedDateString];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:fileName]; //Add the file name
    [pngData writeToFile:filePath atomically:YES]; //Write the file
    NSString *urlString = [[NSString alloc] initWithFormat:@"file://%@", filePath];
    SharePhoto *share = [[SharePhoto alloc] initWithNibName:@"SharePhoto" bundle:nil];
    share.blendedImageURL = urlString;
    share.blendedImageName = fileName;
    
    newImage.image = NULL;
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionNone animations:^{
        [buttonUnderline setTransform:CGAffineTransformMakeTranslation(3, 0)];
    }completion:^(BOOL done){
    }];
    [blendScroll scrollRectToVisible:CGRectMake(0, 0, blendScroll.frame.size.width, blendScroll.frame.size.height) animated:YES];
    [self presentViewController:share animated:YES completion:NULL];
}

- (IBAction)sliderChanged:(id)sender {
    slider = (UISlider *)sender;
    adjustTran.hidden = TRUE;
    
    [overlayView setAlpha:1.0 - slider.value];
    sliderValue = slider.value;
}

- (IBAction)home{
    [self goHomeOrcancel];
}

- (void)goHomeOrcancel{
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:Nil
                                                      message:@"This will delete your current juxt-a-pose"
                                                     delegate:self
                                            cancelButtonTitle:@"Cancel"
                                            otherButtonTitles:@"Delete", nil];
    [message show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"Cancel"])
    {
    }
    else if([title isEqualToString:@"Delete"])
    {
        blendedImage = NULL;
        imageView.image = NULL;
         [self.navigationController popToRootViewControllerAnimated:NO];
    }
}

- (IBAction)cancelPhoto{

    if([firstPhoto isEqualToString:@"first"]){
        [[NSUserDefaults standardUserDefaults] setObject:@"firstCancel" forKey:@"base_photo"];
        [self dismissViewControllerAnimated:NO completion:NULL];
    }else if([firstPhoto isEqualToString:@"cancelUpload"]){
        [[NSUserDefaults standardUserDefaults] setObject:@"second" forKey:@"base_photo"];
        [self dismissViewControllerAnimated:NO completion:NULL];
    }else{
[self dismissViewControllerAnimated:YES completion:NULL];
    }
}


- (IBAction)shoot{
    
    adjustTran.hidden = TRUE;
    [UIView animateWithDuration:0.1 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [bigGreen setTransform:CGAffineTransformMakeTranslation(250, 0)];
        [bigOrange setTransform:CGAffineTransformMakeTranslation(-250, 0)];
    }completion:^(BOOL done){
    }];
    [picker takePicture];
}


- (IBAction)TakePhoto{
   
    [self startCam];
   
}

- (IBAction)ifClicked{
    clickTimes++;
    if (clickTimes == 3){
        clickTimes = 0;
        clickBlink.alpha = 0.2f;
        [UIView animateWithDuration:0.5f
                              delay:0.0f
                            options:UIViewAnimationOptionAutoreverse
                         animations:^
         {
             [UIView setAnimationRepeatCount:6.0f/3.0f];
             clickBlink.alpha = 1.0f;
         }
                         completion:^(BOOL finished)
         {

         }];
        
    }
}
- (IBAction)ifShootClicked{
    clickShootTimes ++;
    if (clickShootTimes == 3){
        clickShootTimes = 0;
        clickShootBlink.alpha = 0.2f;
        [UIView animateWithDuration:0.5f
                              delay:0.0f
                            options:UIViewAnimationOptionAutoreverse
                         animations:^
         {
             [UIView setAnimationRepeatCount:6.0f/3.0f];
             clickShootBlink.alpha = 1.0f;
         }
                         completion:^(BOOL finished)
         {
             
         }];
        
    }
}

-(void) startCam{
    takePicCover.hidden = FALSE;
    [self startTimer];
    picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    
    NSString *flag = [[NSUserDefaults standardUserDefaults] stringForKey:@"not_first_run"];
    if(!flag){
        chooseBlend.hidden = FALSE;
        if([firstPhoto isEqualToString:@"first"]){
            previewImage.image = NULL;
            originalPreviewImage.image = NULL;
            img = NULL;
            tutShoot.hidden = FALSE;
        }else if([firstPhoto isEqualToString:@"second"]){
            tutShoot.hidden = TRUE;
            adjustTran.hidden = FALSE;
            addAnother.hidden = FALSE;
            another.hidden = TRUE;
        }else if([firstPhoto isEqualToString:@"third"]){
            tutShoot.hidden = TRUE;
            adjustTran.hidden = TRUE;
            addAnother.hidden = FALSE;
            saveIt.hidden = FALSE;
            another.hidden = TRUE;
        }
    }else{
        if([firstPhoto isEqualToString:@"first"]){
            previewImage.image = NULL;
            originalPreviewImage.image = NULL;
            img = NULL;
            [blendView removeFromSuperview];
            [blendScrollView removeFromSuperview];
        }
        another.hidden = FALSE;
        tutShoot.hidden = TRUE;
        adjustTran.hidden = TRUE;
        addAnother.hidden = TRUE;
        saveIt.hidden = TRUE;
    }
        
        
   
    
    if(blendedImage){
    newImage=[[UIImageView alloc] init];
    newImage.image=imageView.image;
    }
    
    [picker setSourceType:UIImagePickerControllerSourceTypeCamera];
    picker.showsCameraControls = NO;
    picker.navigationBarHidden = YES;
    picker.toolbarHidden = YES;
    picker.allowsEditing = NO;
    picker.wantsFullScreenLayout = NO;
    picker.toolbar.hidden = YES;
    
    if ([firstPhoto isEqual: @"first"]) {
         [self presentViewController:picker animated:NO completion:NULL];
    }else if ([uploadPhoto isEqual: @"cancelUpload"]) {
        [self presentViewController:picker animated:NO completion:NULL];
    }else{
         [self presentViewController:picker animated:YES completion:NULL];
    }
    
    [self OverlayImageOnTop];
    
}


- (void) OverlayImageOnTop{
    
    picker.showsCameraControls = NO;
    if ([[UIDevice currentDevice] systemVersion].floatValue < 7.0){
    if([[UIScreen mainScreen] bounds].size.height == 568)
    {
        // iPhone 5
        overlayView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 106, 300, 300)];
        slider = [[UISlider alloc] initWithFrame:CGRectMake(33, 419, 254, 16)];
    }
    else
    {
        overlayView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 55, 300, 300)];
        slider = [[UISlider alloc] initWithFrame:CGRectMake(33, 370, 254, 16)];
    }
    }else{
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            // iPhone 5
            overlayView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 106, 300, 300)];
            slider = [[UISlider alloc] initWithFrame:CGRectMake(33, 419, 254, 16)];
        }
        else
        {
            overlayView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 65, 300, 300)];
            slider = [[UISlider alloc] initWithFrame:CGRectMake(33, 372, 254, 16)];
        }
        
    }
    
    UIImage *minImage = [[UIImage imageNamed:@"sliderbgMax.png"] stretchableImageWithLeftCapWidth:5 topCapHeight:5];
    UIImage *maxImage = [[UIImage imageNamed:@"sliderbgMin.png"] stretchableImageWithLeftCapWidth:5 topCapHeight:5];
    UIImage *thumbImage = [UIImage imageNamed:@"sliderThumb.png"];
    UIImageView *thumbButton = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    [thumbButton setImage: thumbImage];
    [[UISlider appearance] setMaximumTrackImage:maxImage forState:UIControlStateNormal];
    [[UISlider appearance] setMinimumTrackImage:minImage forState:UIControlStateNormal];
    [[UISlider appearance] setThumbImage:thumbButton.image forState:UIControlStateNormal];
    [[UISlider appearance] setThumbImage:thumbButton.image forState:UIControlStateHighlighted];
    [[UISlider appearance] setThumbImage:thumbButton.image forState:UIControlStateSelected];
    [slider addTarget:self action:@selector(sliderChanged:) forControlEvents:UIControlEventValueChanged];
    slider.minimumValue = 0.05;
    slider.maximumValue = 0.95;
    slider.continuous = YES;
    
    if([[UIScreen mainScreen] bounds].size.height == 568) // iPhone 5 V 6.0
    {
        bigGreen = [[UIImageView alloc]initWithFrame:CGRectMake(-400, 60, 400, 400)];
        bigGreen.image = [UIImage imageNamed:@"bigGreen.png"];
        bigOrange = [[UIImageView alloc]initWithFrame:CGRectMake(320, 60, 400, 400)];
        bigOrange.image = [UIImage imageNamed:@"bigOrange.png"];
    }else{
    bigGreen = [[UIImageView alloc]initWithFrame:CGRectMake(-400, 0, 400, 400)];
    bigGreen.image = [UIImage imageNamed:@"bigGreen.png"];
    bigOrange = [[UIImageView alloc]initWithFrame:CGRectMake(320, 0, 400, 400)];
    bigOrange.image = [UIImage imageNamed:@"bigOrange.png"];
    }
    
    slider.value = 0.5;
    if ([firstPhoto isEqual: @"first"]) {
        slider.hidden = TRUE;
        transparent.hidden = TRUE;
        opaque.hidden = TRUE;
    }else if ([firstPhoto isEqual: @"new"]) {
        slider.hidden = TRUE;
        transparent.hidden = TRUE;
        opaque.hidden = TRUE;
    }else{
        slider.hidden = FALSE;
        transparent.hidden = FALSE;
        opaque.hidden = FALSE;
    }
   // UIView *pickerView = picker.view;
    [overlayView setImage: newImage.image];
    [overlayView setContentMode:UIViewContentModeScaleToFill];
    realOverlayView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    [realOverlayView addSubview:overlayView];
    [realOverlayView insertSubview:bigGreen aboveSubview:overlayView];
    [realOverlayView insertSubview:bigOrange aboveSubview:bigGreen];
    [realOverlayView insertSubview:camBG aboveSubview:bigOrange];
    [overlayView setAlpha:.5];
    [realOverlayView insertSubview:slider aboveSubview:camBG];
    
    picker.cameraOverlayView = realOverlayView;
    
    flashStatus = [[NSUserDefaults standardUserDefaults] stringForKey:@"flash_mode"];
    if([flashStatus isEqualToString: @"flashAuto"]){
        [picker setCameraFlashMode:UIImagePickerControllerCameraFlashModeAuto];
        [toggleFlashMode setBackgroundImage:[UIImage imageNamed:@"flashAuto.png"] forState:UIControlStateNormal];
    }else if([flashStatus isEqualToString: @"flashOn"]){
        [picker setCameraFlashMode:UIImagePickerControllerCameraFlashModeOn];
        [toggleFlashMode setBackgroundImage:[UIImage imageNamed:@"flashOn.png"] forState:UIControlStateNormal];
    }else if([flashStatus isEqualToString: @"flashOff"]){
        [picker setCameraFlashMode:UIImagePickerControllerCameraFlashModeOff];
        [toggleFlashMode setBackgroundImage:[UIImage imageNamed:@"flashOff.png"] forState:UIControlStateNormal];
    }
}

-(void)startTimer
{
    timer = 0;
    shootTimer = [NSTimer scheduledTimerWithTimeInterval:1.2 target:self selector:@selector(myFunctionForClickImage) userInfo:nil repeats:YES];
}

-(void)myFunctionForClickImage
{
    timer ++;
    if (timer < 1)
    {
        
    }else{
        takePicCover.hidden = TRUE;
    }
}

- (IBAction)ChooseExisting{
    if([firstPhoto isEqualToString:@"first"]){
         [[NSUserDefaults standardUserDefaults] setObject:@"firstUpload" forKey:@"base_photo"];
    }else{
         [[NSUserDefaults standardUserDefaults] setObject:@"upload" forKey:@"base_photo"];
    }

    //PhotoLibrary *cameraRoll = [[PhotoLibrary alloc]initWithNibName:@"PhotoLibrary" bundle:nil];
    //[realOverlayView insertSubview:cameraRoll aboveSubview:slider];
     [self dismissViewControllerAnimated:NO completion:NULL];
}

- (void) imagePickerController:(UIImagePickerController *)pickers didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [bigGreen setTransform:CGAffineTransformMakeTranslation(-250, 0)];
        [bigOrange setTransform:CGAffineTransformMakeTranslation(250, 0)];
    }completion:^(BOOL done){
    }];
        image = [info objectForKey:UIImagePickerControllerOriginalImage];
        [self scaleAndRotateImage:image];
}


- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:NULL];
}



- (void)scaleAndRotateImage:(UIImage *)rotatedImage
{
    imageCopy = [[UIImage alloc]init];
    int kMaxResolution = 640; // Or whatever
    
    CGImageRef imgRef = rotatedImage.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    bounds = CGRectMake(0, 0, width, height);
    
    
    
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = rotatedImage.imageOrientation;
    if(cameraRotation == 0){ //rear camera
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height =bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
        UIGraphicsBeginImageContext(bounds.size);
        
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        
        if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
            CGContextScaleCTM(context, -scaleRatio, scaleRatio);
            CGContextTranslateCTM(context, -height, 0);
        }
        else {
            CGContextScaleCTM(context, scaleRatio, -scaleRatio);
            CGContextTranslateCTM(context, 0, -height);
        }
        
        
        CGContextConcatCTM(context, transform);
        
        CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
        imageCopy = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
    }else if(cameraRotation == 1){ //front camera
        switch(orient) {
                
            case UIImageOrientationUp: //EXIF = 1
                boundHeight = bounds.size.height;
                bounds.size.height = bounds.size.width;
                bounds.size.width = boundHeight;
                transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.height);
                transform = CGAffineTransformScale(transform, -1.0, 1.0);
                transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
                break;
          
            case UIImageOrientationDown: //EXIF = 3
                boundHeight = bounds.size.height;
                bounds.size.height = bounds.size.width;
                bounds.size.width = boundHeight;
                transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.height);
                transform = CGAffineTransformScale(transform, -1.0, 1.0);
                transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
                break;
                
            case UIImageOrientationLeft: //EXIF = 6 orientation is ok but size is off
                boundHeight = bounds.size.height;
                bounds.size.height = bounds.size.width;
                bounds.size.width = boundHeight;
                transform = CGAffineTransformMakeScale(-1.0, 1.0);
                transform = CGAffineTransformRotate(transform, M_PI_2);
                break;
                
            case UIImageOrientationRight: //EXIF = 8 orientation is good size is off
                boundHeight = bounds.size.height;
                bounds.size.height = bounds.size.width;
                bounds.size.width = boundHeight;
                transform = CGAffineTransformMakeScale(-1.0, 1.0);
                transform = CGAffineTransformRotate(transform, M_PI_2);
                break;
                
            default:
                [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
                
        }
        
        
        UIGraphicsBeginImageContext(bounds.size);
        
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        
        if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
            CGContextScaleCTM(context, -scaleRatio, scaleRatio);
            CGContextTranslateCTM(context, -height, 0);
        }
        else {
            CGContextScaleCTM(context, scaleRatio, -scaleRatio);
            CGContextTranslateCTM(context, 0, -height);
        }
        
        
        CGContextConcatCTM(context, transform);
        
        CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
        imageCopy = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    
   
    
    [self setRotatedImage:imageCopy];
    //return imageCopy;
}

/*- (IBAction)videoButton{
    [UIView transitionWithView:videoView
                      duration:0.5
                       options:UIViewAnimationOptionCurveLinear //any animation
                    animations:^ { videoView.transform = CGAffineTransformMakeTranslation(0, -182); }
                    completion:nil];
    
    picker.mediaTypes =[NSArray arrayWithObject:(NSString *)kUTTypeMovie];
    
}

- (IBAction)backToImageCapture{
    [UIView transitionWithView:videoView
                      duration:0.5
                       options:UIViewAnimationOptionCurveLinear //any animation
                    animations:^ { videoView.transform = CGAffineTransformMakeTranslation(0, 0); }
                    completion:nil];
    
    picker.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeImage];
    
}

- (IBAction)startVideo{
    NSLog(@"starting");
    time = 0.00;
    videoTimeAmount = 0;
    videoTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(stopVideoWhenTimeIsUp) userInfo:nil repeats:YES];
    [picker startVideoCapture];
}

-(IBAction)stopVideo{
    if (videoTimeAmount < 5.00){
    NSLog(@"stopped");
    [picker stopVideoCapture];
    }
    
}

-(void)stopVideoWhenTimeIsUp
{
    videoTimeAmount = videoTimeAmount+ 0.1;
    if (videoTimeAmount < 5.000000)
        
    {
        NSLog(@"video time amount: %f", videoTimeAmount);
    }else{
        
        [picker stopVideoCapture];
        [videoTimer invalidate];
    }
}*/

- (void)previewImage: (UIImage *)imagePreview{
    
    /* pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(scale:)];
	[pinch setDelegate:self];
	[previewImage addGestureRecognizer:pinch]; */
    if (blendedImage){
        originalPreviewImage.hidden = FALSE;
        originalPreviewImage.image = blendedImage;
        [originalPreviewImage setAlpha:1.0 - sliderValue];
    }
    
    previewImage.transform = CGAffineTransformMakeRotation(M_PI * 2.0);
    previewView.tag = 7;
    [realOverlayView insertSubview:previewView aboveSubview:slider];
    takenPhoto = imagePreview;
    if ([firstPhoto isEqual: @"first"]) {
        [previewImage setAlpha:1.0];
        //UIImageView *coverBgWhite = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320,480)];
        //coverBgWhite.backgroundColor = [UIColor whiteColor];
        //coverBg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 20, 320, 460)];
        //coverBg.image = [UIImage imageNamed:@"camBgFour.png"];
        //[coverPreview addSubview:coverBgWhite];
        //[coverPreview insertSubview:coverBg aboveSubview:coverBgWhite];
    }else if([firstPhoto isEqual: @"new"]) {
        [previewImage setAlpha:1.0];
    }else{
        overlayAlpha = overlayView.alpha;
        [overlayView setAlpha:1.0];
        //[previewImage setAlpha:slider.value];
        slider.hidden = TRUE;
        transparent.hidden = TRUE;
        opaque.hidden = TRUE;
        
    }
        
    [previewImage setImage:imagePreview];
    //imageCopy = imagePreview;
    //img = imagePreview;
    //[coverPreview insertSubview:previewImage belowSubview:coverBg];
}



-(IBAction)cancelPreview{
    imageRotateCounter = 1;
    [overlayView setAlpha: overlayAlpha];
    NSString *flag = [[NSUserDefaults standardUserDefaults] stringForKey:@"base_photo"];
    if ([flag isEqualToString:@"first"]) {
        slider.hidden = TRUE;
        transparent.hidden = TRUE;
        opaque.hidden = TRUE;
    }else{
        slider.hidden = FALSE;
        transparent.hidden = FALSE;
        opaque.hidden = FALSE;
    }
    //[previewImage removeFromSuperview];
            [previewView removeFromSuperview];
            

}


-(IBAction)usePhoto{
    imageRotateCounter = 1;
    cameraRotation = 0;
    if([firstPhoto isEqualToString:@"second"]){
    [[NSUserDefaults standardUserDefaults] setObject:@"third" forKey:@"base_photo"];
        
    }else if([firstPhoto isEqualToString:@"first"]){
        [[NSUserDefaults standardUserDefaults] setObject:@"second" forKey:@"base_photo"];
        
    }
    //[self setRotatedImage:takenPhoto];
    [self setRotatedImageFirst:takenPhoto];
    sliderValue = slider.value;
}

- (void)setRotatedImageFirst:(UIImage *)rotatedImage{
    
    UIImageView* bottomView = [[UIImageView alloc] initWithImage:rotatedImage];
     topView = [[UIImageView alloc] initWithImage:newImage.image];
     [topView setAlpha:1.0 - slider.value];
     [bottomView addSubview:topView];
     UIGraphicsBeginImageContext(bottomView.frame.size);
     //CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeScreen);
     [bottomView.layer renderInContext:UIGraphicsGetCurrentContext()];
     blendedImage = UIGraphicsGetImageFromCurrentImageContext();
     UIGraphicsEndImageContext();
    //blendedImage = img;
    //firstPhoto = @"anotherPhoto";
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (void)setRotatedImage:(UIImage *)rotatedImage{
    
    UIImage *imageSmall = rotatedImage;
    NSLog(@"image width: %f image height: %f", rotatedImage.size.width, rotatedImage.size.height);
    int fivesxoffset = 10 * 8;
    int fivesyoffset = 102 * 8;
    int imageWidth = rotatedImage.size.width;
    int imageHeight = rotatedImage.size.height;
    if ([[UIDevice currentDevice] systemVersion].floatValue < 7.0){
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            // iPhone 5 V 6.0
            img = [self cropImage:imageSmall andFrame:CGRectMake(48, 190, 543, 543)];
        }
        else
        {
            img = [self cropImage:imageSmall andFrame:CGRectMake(18, 110, 600, 600)];
        }
    }else{
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            // iPhone 5 V 7.0
            img = [self cropImage:imageSmall andFrame:CGRectMake(fivesxoffset, fivesyoffset, imageWidth - (fivesxoffset *2), imageWidth - (fivesxoffset *2))];
        }
        else
        {
            img = [self cropImage:imageSmall andFrame:CGRectMake(50, 109, 543, 543)];
        }
    }
    //[self previewImage:img];
    EditPreviewView *PreviewTheImage = [[EditPreviewView alloc]initWithNibName:@"EditPreviewView" bundle:Nil];
    [self presentViewController:PreviewTheImage animated:YES completion:NULL];
}

- (IBAction)setupBlendScreen{

    [self.view addSubview:blendView];
    
    blendTheBottomView = [[UIImageView alloc] initWithImage:img];
    blendTheTopView = [[UIImageView alloc] initWithImage:newImage.image];
    [blendTheTopView setAlpha:1.0 - sliderValue];
    [blendTheBottomView addSubview:blendTheTopView];
    [self blendNormal];
}

- (IBAction)cancelBlendMode{
    blendTheBottomView = nil;
    blendTheTopView = nil;
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionNone animations:^{
        [buttonUnderline setTransform:CGAffineTransformMakeTranslation(3, 0)];
    }completion:^(BOOL done){
    }];
    [blendScroll scrollRectToVisible:CGRectMake(0, 0, blendScroll.frame.size.width, blendScroll.frame.size.height) animated:YES];
    [blendView  removeFromSuperview];
}

- (void)configureScrollView {
    
    CGSize size = blendScrollView.bounds.size;
    blendScrollView.frame = CGRectMake(0, 0, size.width, size.height);
    [blendScroll addSubview:blendScrollView];
    blendScroll.contentSize = size;
    blendScroll.delegate = self;
    // If you don't use self.contentView anywhere else, clear it here.
    blendScrollView = nil;
    [self blendNormal];
    // If you use it elsewhere, clear it in `dealloc` and `viewDidUnload`.
}

- (IBAction)saveBlended{
    [blendView removeFromSuperview];
    blendView = nil;
    [self dismissViewControllerAnimated:YES completion:NULL];
    
}

#pragma mark - blend Modes


-(IBAction)blendIt:(id)sender{
//[sender setEnabled:NO];
[UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
    if (blendModeBox.transform.ty == 0) {
        [blendModeBox setTransform:CGAffineTransformMakeTranslation(0, -160)];
    }else{
        [blendModeBox setTransform:CGAffineTransformMakeTranslation(0, 0)];
    }
}completion:^(BOOL done){
    //some completion handler
    //[sender setEnabled:YES];
}];
}

- (void)blendNormal{
    UIGraphicsBeginImageContext(blendTheBottomView.frame.size);
    //CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeNormal);
    [blendTheBottomView.layer renderInContext:UIGraphicsGetCurrentContext()];
    blendedImage = UIGraphicsGetImageFromCurrentImageContext();
    [combinedView setImage:blendedImage];
    UIGraphicsEndImageContext();
}

- (IBAction)blendBackToNormal{
    UIGraphicsBeginImageContext(blendTheBottomView.frame.size);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeNormal);
    [blendTheBottomView.layer renderInContext:UIGraphicsGetCurrentContext()];
    blendedImage = UIGraphicsGetImageFromCurrentImageContext();
    [combinedView setImage:blendedImage];
    UIGraphicsEndImageContext();
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionNone animations:^{
        [buttonUnderline setTransform:CGAffineTransformMakeTranslation(3, 0)];
    }completion:^(BOOL done){
    }];
}

- (IBAction)blendMultiply{
    UIGraphicsBeginImageContext(blendTheBottomView.frame.size);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeMultiply);
    [blendTheBottomView.layer renderInContext:UIGraphicsGetCurrentContext()];
    blendedImage = UIGraphicsGetImageFromCurrentImageContext();
    [combinedView setImage:blendedImage];
    UIGraphicsEndImageContext();
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionNone animations:^{
        [buttonUnderline setTransform:CGAffineTransformMakeTranslation(60, 0)];
    }completion:^(BOOL done){
    }];
}

- (IBAction)blendColorBurn{
    UIGraphicsBeginImageContext(blendTheBottomView.frame.size);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeColorBurn);
    [blendTheBottomView.layer renderInContext:UIGraphicsGetCurrentContext()];
    blendedImage = UIGraphicsGetImageFromCurrentImageContext();
    [combinedView setImage:blendedImage];
    UIGraphicsEndImageContext();
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionNone animations:^{
        [buttonUnderline setTransform:CGAffineTransformMakeTranslation(122, 0)];
    }completion:^(BOOL done){
    }];
}

- (IBAction)blendColorDodge{
    UIGraphicsBeginImageContext(blendTheBottomView.frame.size);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeColorDodge);
    [blendTheBottomView.layer renderInContext:UIGraphicsGetCurrentContext()];
    blendedImage = UIGraphicsGetImageFromCurrentImageContext();
    [combinedView setImage:blendedImage];
    UIGraphicsEndImageContext();
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionNone animations:^{
        [buttonUnderline setTransform:CGAffineTransformMakeTranslation(660, 0)];
    }completion:^(BOOL done){
    }];
}

- (IBAction)blendLuminosity{
    UIGraphicsBeginImageContext(blendTheBottomView.frame.size);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeLuminosity);
    [blendTheBottomView.layer renderInContext:UIGraphicsGetCurrentContext()];
    blendedImage = UIGraphicsGetImageFromCurrentImageContext();
    [combinedView setImage:blendedImage];
    UIGraphicsEndImageContext();
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionNone animations:^{
        [buttonUnderline setTransform:CGAffineTransformMakeTranslation(720, 0)];
    }completion:^(BOOL done){
    }];
}

- (IBAction)blendDarken{
    UIGraphicsBeginImageContext(blendTheBottomView.frame.size);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeDarken);
    [blendTheBottomView.layer renderInContext:UIGraphicsGetCurrentContext()];
    blendedImage = UIGraphicsGetImageFromCurrentImageContext();
    [combinedView setImage:blendedImage];
    UIGraphicsEndImageContext();
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionNone animations:^{
        [buttonUnderline setTransform:CGAffineTransformMakeTranslation(240, 0)];
    }completion:^(BOOL done){
    }];
}

- (IBAction)blendDifference{
    UIGraphicsBeginImageContext(blendTheBottomView.frame.size);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeDifference);
    [blendTheBottomView.layer renderInContext:UIGraphicsGetCurrentContext()];
    blendedImage = UIGraphicsGetImageFromCurrentImageContext();
    [combinedView setImage:blendedImage];
    UIGraphicsEndImageContext();
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionNone animations:^{
        [buttonUnderline setTransform:CGAffineTransformMakeTranslation(300, 0)];
    }completion:^(BOOL done){
    }];
}

- (IBAction)blendExclusion{
    UIGraphicsBeginImageContext(blendTheBottomView.frame.size);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeExclusion);
    [blendTheBottomView.layer renderInContext:UIGraphicsGetCurrentContext()];
    blendedImage = UIGraphicsGetImageFromCurrentImageContext();
    [combinedView setImage:blendedImage];
    UIGraphicsEndImageContext();
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionNone animations:^{
        [buttonUnderline setTransform:CGAffineTransformMakeTranslation(360, 0)];
    }completion:^(BOOL done){
    }];
}

- (IBAction)blendHardlight{
    UIGraphicsBeginImageContext(blendTheBottomView.frame.size);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeHardLight);
    [blendTheBottomView.layer renderInContext:UIGraphicsGetCurrentContext()];
    blendedImage = UIGraphicsGetImageFromCurrentImageContext();
    [combinedView setImage:blendedImage];
    UIGraphicsEndImageContext();
        [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionNone animations:^{
        [buttonUnderline setTransform:CGAffineTransformMakeTranslation(540, 0)];
    }completion:^(BOOL done){
    }];
}

- (IBAction)blendOverlay{
    UIGraphicsBeginImageContext(blendTheBottomView.frame.size);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeOverlay);
    [blendTheBottomView.layer renderInContext:UIGraphicsGetCurrentContext()];
    blendedImage = UIGraphicsGetImageFromCurrentImageContext();
    [combinedView setImage:blendedImage];
    UIGraphicsEndImageContext();
        [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionNone animations:^{
        [buttonUnderline setTransform:CGAffineTransformMakeTranslation(419, 0)];
    }completion:^(BOOL done){
    }];
}

- (IBAction)blendScreen{
    UIGraphicsBeginImageContext(blendTheBottomView.frame.size);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeScreen);
    [blendTheBottomView.layer renderInContext:UIGraphicsGetCurrentContext()];
    blendedImage = UIGraphicsGetImageFromCurrentImageContext();
    [combinedView setImage:blendedImage];
    UIGraphicsEndImageContext();
        [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionNone animations:^{
        [buttonUnderline setTransform:CGAffineTransformMakeTranslation(480, 0)];
    }completion:^(BOOL done){
    }];
}

- (IBAction)blendSaturation{
    UIGraphicsBeginImageContext(blendTheBottomView.frame.size);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeSaturation);
    [blendTheBottomView.layer renderInContext:UIGraphicsGetCurrentContext()];
    blendedImage = UIGraphicsGetImageFromCurrentImageContext();
    [combinedView setImage:blendedImage];
    UIGraphicsEndImageContext();
        [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionNone animations:^{
        [buttonUnderline setTransform:CGAffineTransformMakeTranslation(600, 0)];
    }completion:^(BOOL done){
    }];
}

- (IBAction)blendHue{
    UIGraphicsBeginImageContext(blendTheBottomView.frame.size);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeHue);
    [blendTheBottomView.layer renderInContext:UIGraphicsGetCurrentContext()];
    blendedImage = UIGraphicsGetImageFromCurrentImageContext();
    [combinedView setImage:blendedImage];
    UIGraphicsEndImageContext();
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionNone animations:^{
        [buttonUnderline setTransform:CGAffineTransformMakeTranslation(841, 0)];
    }completion:^(BOOL done){
    }];
}

- (IBAction)blendColor{
    UIGraphicsBeginImageContext(blendTheBottomView.frame.size);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeColor);
    [blendTheBottomView.layer renderInContext:UIGraphicsGetCurrentContext()];
    blendedImage = UIGraphicsGetImageFromCurrentImageContext();
    [combinedView setImage:blendedImage];
    UIGraphicsEndImageContext();
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionNone animations:^{
        [buttonUnderline setTransform:CGAffineTransformMakeTranslation(900, 0)];
    }completion:^(BOOL done){
    }];
}

- (IBAction)blendDarker{
    UIGraphicsBeginImageContext(blendTheBottomView.frame.size);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModePlusDarker);
    [blendTheBottomView.layer renderInContext:UIGraphicsGetCurrentContext()];
    blendedImage = UIGraphicsGetImageFromCurrentImageContext();
    [combinedView setImage:blendedImage];
    UIGraphicsEndImageContext();
        [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionNone animations:^{
        [buttonUnderline setTransform:CGAffineTransformMakeTranslation(180, 0)];
    }completion:^(BOOL done){
    }];
}

- (IBAction)blendLighter{
    UIGraphicsBeginImageContext(blendTheBottomView.frame.size);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModePlusLighter);
    [blendTheBottomView.layer renderInContext:UIGraphicsGetCurrentContext()];
    blendedImage = UIGraphicsGetImageFromCurrentImageContext();
    [combinedView setImage:blendedImage];
    UIGraphicsEndImageContext();
        [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionNone animations:^{
        [buttonUnderline setTransform:CGAffineTransformMakeTranslation(240, 0)];
    }completion:^(BOOL done){
    }];
}

- (IBAction)blendLighten{
    UIGraphicsBeginImageContext(blendTheBottomView.frame.size);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeLighten);
    [blendTheBottomView.layer renderInContext:UIGraphicsGetCurrentContext()];
    blendedImage = UIGraphicsGetImageFromCurrentImageContext();
    [combinedView setImage:blendedImage];
    UIGraphicsEndImageContext();
}

- (IBAction)blendSoftLight{
    UIGraphicsBeginImageContext(blendTheBottomView.frame.size);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(), kCGBlendModeSoftLight);
    [blendTheBottomView.layer renderInContext:UIGraphicsGetCurrentContext()];
    blendedImage = UIGraphicsGetImageFromCurrentImageContext();
    [combinedView setImage:blendedImage];
    UIGraphicsEndImageContext();
        [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionNone animations:^{
        [buttonUnderline setTransform:CGAffineTransformMakeTranslation(780, 0)];
    }completion:^(BOOL done){
    }];
}

- (void)blendUnderlinePosition:(float)xPosition blendUnderlineWidth:(float)buttonWidth{
    
    //float underlineWidth = buttonUnderline.frame.size.width;
    //float additionalDistance = (buttonWidth / 2) - (underlineWidth /2);
    //float underlinePosition = buttonUnderline.frame.origin.x;
    //float distanceToMove = (xPosition + additionalDistance) - underlinePosition;
    
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionNone animations:^{
        [buttonUnderline setTransform:CGAffineTransformMakeTranslation(((xPosition) *2)-8, 0)];
    }completion:^(BOOL done){
    }];
}
#pragma mark - gesture controls

/*-(void)pinchRotate:(UIRotationGestureRecognizer*)rotate
{
    switch (rotate.state)
    {
        case UIGestureRecognizerStateBegan:
        {

                AcuRotation = 0.0;
 
            break;


        }
        case UIGestureRecognizerStateChanged:
        {
            
            //            CGAffineTransform transform;
            //            [UIView beginAnimations:nil context:NULL];
            //            [UIView setAnimationDuration:1.0];
            //            [UIView setAnimationCurve:UIViewAnimationOptionBeginFromCurrentState];
            //            self.view.alpha = 1;
            //            transform = CGAffineTransformRotate(self.view.transform,0.5*M_PI);
            //            [self.view setUserInteractionEnabled:YES];
            //            self.view.transform = transform;
            //            [UIView commitAnimations];
            
            float thisRotate = rotate.rotation - AcuRotation;
            AcuRotation = rotate.rotation;
            previewImage.transform = CGAffineTransformRotate(previewImage.transform, thisRotate);
            _lastRotation = _lastRotation + thisRotate;
            //NSLog(@"rot:%f",(_lastRotation * 180) / M_PI);
            NSLog(@"rot cos:%f",(cos(_lastRotation*4)+3)/2);
           // NSLog(@"sin:%f",(cos(M_PI)));
            
            break;

            
        }
            
        case UIGestureRecognizerStateEnded:
        {
            

            break;


        }
        case UIGestureRecognizerStateFailed:{
            break;

        }
            
        default:
            break;
    }
    
}



-(void)scale:(UIPinchGestureRecognizer*)scaler
{
    switch (scaler.state)
    {
        case UIGestureRecognizerStateBegan:
            
        {
            
                AcuScale = 1.0;

            
            break;

        }
        case UIGestureRecognizerStateChanged:
        {
            _lastScale = [scaler scale];
            float thisScale = 1 + (scaler.scale-AcuScale);
            AcuScale = scaler.scale;
            previewImage.transform = CGAffineTransformScale(previewImage.transform, thisScale, thisScale);
            
            
            //NSLog(@"this is scale:%f",[scaler scale]);
            //            CGAffineTransform transform;
            //            [UIView beginAnimations:nil context:NULL];
            //            [UIView setAnimationDuration:1.0];
            //            [UIView setAnimationCurve:UIViewAnimationOptionBeginFromCurrentState];
            //            self.view.alpha = 1;
            //            transform = CGAffineTransformRotate(self.view.transform,0.5*M_PI);
            //            [self.view setUserInteractionEnabled:YES];
            //            self.view.transform = transform;
            //            [UIView commitAnimations];
            //_lastRotation = [rotate rotation];
            //[self rotate:[rotate rotation]];
            break;

            
        }
        case UIGestureRecognizerStateEnded:
        {
            scaling = FALSE;
            break;


        
        }
        case UIGestureRecognizerStateFailed:{
        }
            
        default:
            break;
    }
    
}

-(void)press:(UILongPressGestureRecognizer*)tapper{
    //NSLog(@"GESTURE IS:%d",tapper.state);
    switch (tapper.state)
    {
        case UIGestureRecognizerStateBegan:
            
        {
            break;

        }
        case UIGestureRecognizerStateChanged:
        {
            break;


            
        }
        case UIGestureRecognizerStateEnded:
        {

            [self fixTransform];
            break;

            
        }
        case UIGestureRecognizerStateFailed:{

        }
            
        default:
            break;
    }
}


-(void)move:(UIPanGestureRecognizer*)pan{
    //NSLog(@"GESTURE IS:%d",tapper.state);
    switch (pan.state)
    {
        case UIGestureRecognizerStateBegan:
            
        {
           // NSLog(@"this is anchor %f, %f",previewImage.layer.anchorPoint.x,previewImage.layer.anchorPoint.y);
           // NSLog(@"this is the pan %f and %f",[pan translationInView:previewView].x,[pan translationInView:previewView].y);
                movePoint = [pan locationInView:previewView];
            break;
            
        }
        case UIGestureRecognizerStateChanged:
        {
            //NSLog(@"this is the pan %f and %f",[pan translationInView:previewView].x,[pan translationInView:previewView].y);
            CGPoint curr = [pan locationInView:previewView];
            
            float diffx = curr.x - movePoint.x;
            float diffy = curr.y - movePoint.y;
            
            CGPoint centre = previewImage.center;
            centre.x += diffx;
            centre.y += diffy;
            previewImage.center = centre;
            movePoint = curr;
            
        }
        case UIGestureRecognizerStateEnded:
        {
            
            break;
            
            
        }
        case UIGestureRecognizerStateFailed:{
            
        }
            
        default:
            break;
    }
}

*/


-(void)transformTheImage{
//
//    [UIView beginAnimations:nil context:NULL];
//    [UIView setAnimationDuration: 0.1];
//    [UIView setAnimationDelegate:self];
//    previewImage.contentMode = UIViewContentModeScaleAspectFit;
//    AcuScale = AcuScale + (_lastScale-AcuScale);
//    AcuRotation = AcuRotation + _lastRotation;
//   // NSLog(@"This is scale %f",AcuScale);
//    //NSLog(@"this is rotate:%f and Scale:%f accum",AcuRotation, AcuScale);
//    CGAffineTransform rotateit = CGAffineTransformMakeRotation(_lastRotation);
//    CGAffineTransform scaleit = CGAffineTransformMakeScale(AcuScale, AcuScale);
//    CGAffineTransform moveit = CGAffineTransformMakeTranslation(movePoint.x, movePoint.y);
//    //NSLog(@"rot: %f",(cos(_lastRotation*4)+1)/4);
//    CGAffineTransform allTrans = CGAffineTransformConcat(rotateit, scaleit);
//    allTrans = CGAffineTransformConcat(allTrans, moveit);
//    previewImage.transform = allTrans;
//    [UIView commitAnimations];
//    previewImage.frame = CGRectMake(movePoint.x, movePoint.y, previewImage.frame.size.width*_lastScale, previewImage.frame.size.height*_lastScale);

    
}

-(void)fixTransform{

    
    NSLog(@"rot: %f",_lastRotation);

    if(previewImage.frame.size.width<300 || previewImage.frame.size.height<300){
        CGFloat newScale = (300*((cos((_lastRotation-(M_PI/4))*4)+3)/2))/previewImage.frame.size.width;
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration: 0.1];
        [UIView setAnimationDelegate:self];
        previewImage.transform = CGAffineTransformScale(previewImage.transform, newScale, newScale);
        [UIView commitAnimations];
         NSLog(@"this is new Scale %f",newScale);
    }
    float ix = (previewImage.center.x - (previewImage.frame.size.width/2))-frameX;
    float iy = (previewImage.center.y - (previewImage.frame.size.height/2))-frameY;

    NSLog(@"this is the position %f, %f this is the rotation %f, and scale: %f",ix,iy,AcuRotation,AcuScale);
    NSLog(@"this is the frame x,y %f, %f",previewImage.frame.size.width,previewImage.frame.size.height);
    
    if(previewImage.frame.size.width<300 || previewImage.frame.size.height<300){
    
    }else if(ix>0 || iy>0 || (iy+previewImage.frame.size.height < 300) || (ix+previewImage.frame.size.width < 300)){
        CGPoint centre = previewImage.center;
        if(ix>0 || ix+previewImage.frame.size.width < 300){
            centre.x = (previewImage.frame.size.width/2)+frameX;
        }
        if(iy>0 || iy+previewImage.frame.size.height < 300){
            centre.y = (previewImage.frame.size.height/2)+frameY;
        }
        previewImage.center = centre;
        
    }

//    if(AcuScale < 1.55-((cos(AcuRotation*4)+1)/4)){
//    AcuScale = 1.55-((cos(AcuRotation*4)+1)/4);
//    previewImage.transform = CGAffineTransformScale(previewImage.transform, AcuScale, thisScale);
//    }
    
}





//-(void)scale:(id)sender {
// 
// if([(UIPinchGestureRecognizer*)sender state] == UIGestureRecognizerStateBegan) {
// _lastScale = 1.0;
// NSLog(@"scaling %f", _lastScale);
// }
// CGFloat scale = 1.0 - (_lastScale - [(UIPinchGestureRecognizer*)sender scale]);
// 
// CGAffineTransform currentTransform = previewImage.transform;
// CGAffineTransform newTransform = CGAffineTransformScale(currentTransform, scale, scale);
// 
// [previewImage setTransform:newTransform];
// 
// _lastScale = [(UIPinchGestureRecognizer*)sender scale];
// NSLog(@"last Scale %f", _lastScale);
// //[self showOverlayWithFrame:photoImage.frame];
// }


 
// -(void)move:(id)sender {
// 
// CGPoint translatedPoint = [(UIPanGestureRecognizer*)sender translationInView:previewImage];
// 
// if([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateBegan) {
// _firstX = [previewImage center].x;
// _firstY = [previewImage center].y;
// NSLog(@"moving x %f", _firstX);
// NSLog(@"moving y %f", _firstY);
// }
// 
// translatedPoint = CGPointMake(_firstX+translatedPoint.x, _firstY+translatedPoint.y);
// NSLog(@"translated Point x %f", translatedPoint.x);
// NSLog(@"translated Point y %f", translatedPoint.y);
// [previewImage setCenter:translatedPoint];
// //[self showOverlayWithFrame:originalPreviewImage.frame];
// }

// - (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
//     return TRUE;
// }


 
 
#pragma mark - edit Image Functions


- (IBAction)rotateTakenImage{

    switch (imageRotateCounter) {
        case 1:
            rotateAngle = M_PI_2;
            previewImage.transform = CGAffineTransformMakeRotation(rotateAngle);
            imageRotateCounter++;
            break;
        case 2:
            rotateAngle = M_PI;
            imageRotateCounter++;
            previewImage.transform = CGAffineTransformMakeRotation(rotateAngle);
            rotateAngle = M_PI_2;
            break;
        case 3:
            rotateAngle = 3.0 * M_PI / 2.0;
            previewImage.transform = CGAffineTransformMakeRotation(rotateAngle);
            imageRotateCounter++;
            rotateAngle = M_PI_2;
            break;
        case 4:
            rotateAngle = M_PI * 2;
            previewImage.transform = CGAffineTransformMakeRotation(rotateAngle);
            imageRotateCounter = 1;
            rotateAngle = M_PI / 2.0;
            break;
    }
    
    
    CGSize rotatedSize = img.size;
    
    UIGraphicsBeginImageContext(rotatedSize);
    CGContextRef bitmap = UIGraphicsGetCurrentContext();
    
    if ([[UIDevice currentDevice] systemVersion].floatValue < 7.0){
    if([[UIScreen mainScreen] bounds].size.height == 568)
    {

                 CGContextTranslateCTM(bitmap, previewImage.frame.size.width -28, previewImage.frame.size.height -28);
    }
    else
    {
        CGContextTranslateCTM(bitmap, previewImage.frame.size.width, previewImage.frame.size.height);
    }
    }else{
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            
            CGContextTranslateCTM(bitmap, previewImage.frame.size.width, previewImage.frame.size.height);
        }
        else
        {
            CGContextTranslateCTM(bitmap, previewImage.frame.size.width -29, previewImage.frame.size.height -29);
        }
    }
    
    CGContextRotateCTM(bitmap, rotateAngle);
    CGContextScaleCTM(bitmap, 1.0f, -1.0f);
    CGContextDrawImage(bitmap, CGRectMake(-img.size.width / 2.0f,
                                          -img.size.height / 2.0f,
                                          img.size.width,
                                          img.size.height),
                       img.CGImage);
    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    img = resultImage;
    takenPhoto = img;
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}

- (UIImage *)cropImage:(UIImage*)cropped andFrame:(CGRect)rect {
    
    
    rect = CGRectMake(rect.origin.x*cropped.scale,
                      rect.origin.y*cropped.scale,
                      rect.size.width*cropped.scale,
                      rect.size.height*cropped.scale);
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([cropped CGImage], rect);
    UIImage *result = [UIImage imageWithCGImage:imageRef
                                          scale:cropped.scale
                                    orientation:cropped.imageOrientation];
    CGImageRelease(imageRef);
    return result;
}


#pragma mark - View Methods


- (void)viewDidAppear:(BOOL)animated{
    clickTimes = 0;
    clickShootTimes = 0;
    
    flashStatus = [[NSUserDefaults standardUserDefaults] stringForKey:@"flash_mode"];
    if([flashStatus isEqualToString: @"flashAuto"]){
        [picker setCameraFlashMode:UIImagePickerControllerCameraFlashModeAuto];
        [toggleFlashMode setBackgroundImage:[UIImage imageNamed:@"flashAuto.png"] forState:UIControlStateNormal];
    }else if([flashStatus isEqualToString: @"flashOn"]){
        [picker setCameraFlashMode:UIImagePickerControllerCameraFlashModeOn];
        [toggleFlashMode setBackgroundImage:[UIImage imageNamed:@"flashOn.png"] forState:UIControlStateNormal];
    }else if([flashStatus isEqualToString: @"flashOff"]){
        [picker setCameraFlashMode:UIImagePickerControllerCameraFlashModeOff];
        [toggleFlashMode setBackgroundImage:[UIImage imageNamed:@"flashOff.png"] forState:UIControlStateNormal];
    }
    
    another.alpha = 0.2f;
    [UIView animateWithDuration:0.5f
                          delay:0.0f
                        options:UIViewAnimationOptionAutoreverse
                     animations:^
     {
         [UIView setAnimationRepeatCount:6.0f/3.0f];
         another.alpha = 1.0f;
     }
                     completion:^(BOOL finished)
     {
     }];
    if(blendedImage){
    [imageView setImage:blendedImage];
    imageView.alpha = 0.0f;
    [UIView animateWithDuration:0.5f
                          delay:0.0f
                        options:UIViewAnimationOptionAllowAnimatedContent
                     animations:^
     {
         imageView.alpha = 1.0f;
     }
                     completion:^(BOOL finished)
     {
     }];
    }
    firstPhoto = [[NSUserDefaults standardUserDefaults] stringForKey:@"base_photo"];
    
    if ([firstPhoto isEqualToString:@"first"]){
        
        [self startCam];
    }else if([firstPhoto isEqual: @"second"]) {
        NSString *flag = [[NSUserDefaults standardUserDefaults] stringForKey:@"not_first_run"];
        if(!flag){
                addAnother.hidden = FALSE;
                another.hidden = TRUE;
        }
            saveCover.hidden = FALSE;
    }else if([firstPhoto isEqual: @"third"]) {
        NSString *flag = [[NSUserDefaults standardUserDefaults] stringForKey:@"not_first_run"];
        if(!flag){
            saveIt.hidden = FALSE;
            addAnother.hidden = FALSE;
            another.hidden = TRUE;
            saveCover.hidden = TRUE;
        }else{
         saveCover.hidden = TRUE;    
        }
       
    }else if ([firstPhoto isEqualToString: @"upload"]){
        NSString *flag = [[NSUserDefaults standardUserDefaults] stringForKey:@"not_first_run"];
        if(!flag){
            addAnother.hidden = FALSE;
            saveIt.hidden = FALSE;
            another.hidden = TRUE;
        }
        PhotoLibrary *library = [[PhotoLibrary alloc] initWithNibName:@"PhotoLibrary" bundle:nil];
        [self presentViewController:library animated:NO completion:NULL];
    }else if ([firstPhoto isEqualToString: @"firstUpload"]){
        PhotoLibrary *library = [[PhotoLibrary alloc] initWithNibName:@"PhotoLibrary" bundle:nil];
        [self presentViewController:library animated:NO completion:NULL];
    }else if ([firstPhoto isEqualToString: @"cancelUpload"]){
        [self startCam];
        [[NSUserDefaults standardUserDefaults] setObject:@"second" forKey:@"base_photo"];
    }else if ([firstPhoto isEqualToString: @"firstCancel"]){
        [self.navigationController popToRootViewControllerAnimated:NO];
    }else{
        saveCover.hidden = TRUE;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    arrow.hidden = TRUE;
}

- (void)loadView
{
    if([[UIScreen mainScreen] bounds].size.height == 568)
    {
        // iPhone 5
        self.view = [[NSBundle mainBundle] loadNibNamed:@"ViewController" owner:self options:nil][0];
    }
    else
    {
        self.view = [[NSBundle mainBundle] loadNibNamed:@"ViewControllerFour" owner:self options:nil][0];
        
    }
}

- (void)viewDidLoad
{
    
    [self configureScrollView];
    imageRotateCounter = 1;
    [super viewDidLoad];
    NSLog(@"HI");

    AcuRotation = 0;
    AcuScale = 1;
    cameraRotation = 0;
    PosX = 0;
    PosY = 0;
    frameX = 10;
    frameY = 106;
    

    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    tutShoot.hidden = TRUE;
    adjustTran.hidden = TRUE;
    addAnother.hidden = TRUE;
    saveIt.hidden = TRUE;
    another.hidden = FALSE;
    
    newJuxtaposeImage = [[JxtaPic alloc]init];
    
    
   /* UILongPressGestureRecognizer *longpressSingle = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(press:)];
	[longpressSingle setNumberOfTouchesRequired:1];
    [longpressSingle setMinimumPressDuration:.01];
    [longpressSingle setDelegate:self];
	[previewView addGestureRecognizer:longpressSingle];
    
    UILongPressGestureRecognizer *longpressDouble = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(press:)];
	[longpressDouble setNumberOfTouchesRequired:2];
    [longpressDouble setMinimumPressDuration:.01];
    [longpressDouble setDelegate:self];
	[previewView addGestureRecognizer:longpressDouble];
    
    UIPinchGestureRecognizer *pinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(scale:)];
	[pinchRecognizer setDelegate:self];
	[previewView addGestureRecognizer:pinchRecognizer];
    
	UIRotationGestureRecognizer *rotationRecognizer = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(pinchRotate:)];
	[rotationRecognizer setDelegate:self];
	[previewView addGestureRecognizer:rotationRecognizer];
    
	UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(move:)];
	[panRecognizer setMinimumNumberOfTouches:1];
	[panRecognizer setMaximumNumberOfTouches:1];
	[panRecognizer setDelegate:self];
	[previewView addGestureRecognizer:panRecognizer];
    
    
    

    
    
     previewView.multipleTouchEnabled = YES; */
    
    
}



- (void) topLayerToBlend:(UIImage *)topBlend topLayerAlpha:(float)alpha bottomLayerToBlend:(UIImage *)bottomBlend{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
