//
//  PhotoLibrary.m
//  Juxt-a-pose
//
//  Created by Brandon Phillips on 7/3/13.
//  Copyright (c) 2013 We Are Station. All rights reserved.
//

#import "PhotoLibrary.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"

@interface PhotoLibrary ()

@end

@implementation PhotoLibrary

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (IBAction)sliderChanged:(id)sender {
    slider = (UISlider *)sender;
    adjustTran.hidden = TRUE;
    
    [blendedImageView setAlpha:1.0 - slider.value];
    sliderValue = slider.value;
}

- (IBAction)usePhoto{
    sliderValue = slider.value;
    UIImageView* bottomView = [[UIImageView alloc] initWithImage:img];
    topView = [[UIImageView alloc] initWithImage:blendedImageView.image];
    [topView setAlpha:1.0 - slider.value];
    [bottomView addSubview:topView];
    UIGraphicsBeginImageContext(bottomView.frame.size);
    [bottomView.layer renderInContext:UIGraphicsGetCurrentContext()];
    blendedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    if([firstPhoto isEqualToString:@"firstUploaded"]){
        [[NSUserDefaults standardUserDefaults] setObject:@"second" forKey:@"base_photo"];
    }else{
        [[NSUserDefaults standardUserDefaults] setObject:@"third" forKey:@"base_photo"];
    }
    [self dismissViewControllerAnimated:YES completion:NULL];

}

- (IBAction)cancel{
    
    if([firstPhoto isEqualToString:@"firstUploaded"]){
        [[NSUserDefaults standardUserDefaults] setObject:@"first" forKey:@"base_photo"];
    }else{
        [[NSUserDefaults standardUserDefaults] setObject:@"cancelUpload" forKey:@"base_photo"];
    }
    [self dismissViewControllerAnimated:NO completion:NULL];
}

- (void) goToPhotoLibrary{
    picker2 = [[UIImagePickerController alloc] init];
    picker2.delegate = self;
    [picker2 setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [self presentViewController:picker2 animated:NO completion:NULL];
}

- (void) imagePickerController:(UIImagePickerController *)pickers didFinishPickingMediaWithInfo:(NSDictionary *)info{
    image = [info objectForKey:UIImagePickerControllerOriginalImage];
    [self scaleAndRotateImage:image];
    
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    if([firstPhoto isEqualToString:@"firstUpload"]){
        [[NSUserDefaults standardUserDefaults] setObject:@"first" forKey:@"base_photo"];
    }else{
        [[NSUserDefaults standardUserDefaults] setObject:@"cancelUpload" forKey:@"base_photo"];
    }
    [self dismissViewControllerAnimated:NO completion:NULL];
}

- (void)scaleAndRotateImage:(UIImage *)rotatedImage
{
    int kMaxResolution = 640; // Or whatever
    
    CGImageRef imgRef = rotatedImage.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    bounds = CGRectMake(0, 0, width, height);

        CGFloat ratio = width/height;
        if (ratio < 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = bounds.size.width / ratio;
            //NSLog(@"ratio1");
            //NSLog(@"WIDTH OF THE NEW SIZE IS %f HEIGHT IS %f", bounds.size.width, bounds.size.height);
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = bounds.size.height * ratio;
            //NSLog(@"ratio2");
            //NSLog(@"WIDTH OF THE NEW SIZE IS %f HEIGHT IS %f", bounds.size.width, bounds.size.height);
        }
    
    
    
    
    
    CGFloat scaleRatio = bounds.size.width / width;
    UIImageOrientation orient = rotatedImage.imageOrientation;
            UIGraphicsBeginImageContext(bounds.size);
        
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        
        if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
            CGContextRotateCTM(context, 3.0 * M_PI/2.0);
            CGContextScaleCTM(context, -scaleRatio, scaleRatio);
            //NSLog(@"left");
            
        }
        else {
            
                CGContextScaleCTM(context, scaleRatio, -scaleRatio);
                CGContextTranslateCTM(context, 0, -height);
            
        }
        
        
        CGContextConcatCTM(context, transform);
        
        CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
        imageCopy = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    
   // 
    [self setRotatedImage:imageCopy];
    [self dismissViewControllerAnimated:YES completion:NULL];
    }


- (void)setRotatedImage:(UIImage *)rotatedImage{
    
    UIImage *imageSmall = rotatedImage;
    if ([[UIDevice currentDevice] systemVersion].floatValue < 7.0){
    if([[UIScreen mainScreen] bounds].size.height == 568)
    {
        // iPhone 5
        img = [self cropImage:imageSmall andFrame:CGRectMake(0,0,543, 543)];
    }
    else
    {
        img = [self cropImage:imageSmall andFrame:CGRectMake(0,0, 600, 600)];
    }
    }else{
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            // iPhone 5
            img = [self cropImage:imageSmall andFrame:CGRectMake(0,0,600, 600)];
        }
        else
        {
            img = [self cropImage:imageSmall andFrame:CGRectMake(0,0, 543, 543)];
        }
    }
    if([[UIScreen mainScreen] bounds].size.height == 568)
    {
        // iPhone 5
        slider = [[UISlider alloc] initWithFrame:CGRectMake(33, 419, 254, 16)];
    }
    else
    {
        slider = [[UISlider alloc] initWithFrame:CGRectMake(33, 372, 254, 16)];
    }
    
    UIImage *minImage = [[UIImage imageNamed:@"sliderbgMin.png"] stretchableImageWithLeftCapWidth:4 topCapHeight:0];
    UIImage *maxImage = [[UIImage imageNamed:@"sliderbgMax.png"] stretchableImageWithLeftCapWidth:4 topCapHeight:0];
    UIImage *thumbImage = [UIImage imageNamed:@"sliderThumb.png"];
    UIImageView *thumbButton = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    thumbButton.image = thumbImage;
    [[UISlider appearance] setMaximumTrackImage:maxImage forState:UIControlStateNormal];
    [[UISlider appearance] setMinimumTrackImage:minImage forState:UIControlStateNormal];
    [[UISlider appearance] setThumbImage:thumbButton.image forState:UIControlStateNormal];
    [[UISlider appearance] setThumbImage:thumbButton.image forState:UIControlStateHighlighted];
    [[UISlider appearance] setThumbImage:thumbButton.image forState:UIControlStateSelected];
    [slider addTarget:self action:@selector(sliderChanged:) forControlEvents:UIControlEventValueChanged];
    slider.minimumValue = 0.05;
    slider.maximumValue = 0.95;
    slider.value = 0.5;
    slider.continuous = YES;
    if([firstPhoto isEqualToString:@"firstUpload"]){
        slider.hidden = TRUE;
        transparent.hidden = TRUE;
        opaque.hidden = TRUE;
        [[NSUserDefaults standardUserDefaults] setObject:@"firstUploaded" forKey:@"base_photo"];
    }else if([firstPhoto isEqualToString:@"upload"]){
        [[NSUserDefaults standardUserDefaults] setObject:@"uploaded" forKey:@"base_photo"];
    }else{
        
    }
    [pickedImage setAlpha:1.0];
    pickedImage.transform = CGAffineTransformMakeRotation(M_PI * 2.0);
    [pickedImage setImage:img];
    
    
}

- (UIImage *)cropImage:(UIImage*)cropped andFrame:(CGRect)rect {
    
    //Note : rec is nothing but the image frame which u want to crop exactly.
    
    rect = CGRectMake(rect.origin.x*cropped.scale,
                      rect.origin.y*cropped.scale,
                      rect.size.width*cropped.scale,
                      rect.size.height*cropped.scale);
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([cropped CGImage], rect);
    UIImage *result = [UIImage imageWithCGImage:imageRef
                                          scale:cropped.scale
                                    orientation:cropped.imageOrientation];
    CGImageRelease(imageRef);
    return result;
}

- (IBAction)rotatePickedImage{
    switch (imageRotateCounter) {
        case 1:
            rotateAngle = M_PI_2;
            pickedImage.transform = CGAffineTransformMakeRotation(rotateAngle);
            imageRotateCounter++;
            break;
        case 2:
            rotateAngle = M_PI;
            imageRotateCounter++;
            pickedImage.transform = CGAffineTransformMakeRotation(rotateAngle);
            rotateAngle = M_PI_2;
            break;
        case 3:
            rotateAngle = 3.0 * M_PI / 2.0;
            pickedImage.transform = CGAffineTransformMakeRotation(rotateAngle);
            imageRotateCounter++;
            rotateAngle = M_PI_2;
            break;
        case 4:
            rotateAngle = M_PI * 2;
            pickedImage.transform = CGAffineTransformMakeRotation(rotateAngle);
            imageRotateCounter = 1;
            rotateAngle = M_PI / 2.0;
            break;
    }
    
    
    CGSize rotatedSize = img.size;
    
    UIGraphicsBeginImageContext(rotatedSize);
    CGContextRef bitmap = UIGraphicsGetCurrentContext();
    
    if ([[UIDevice currentDevice] systemVersion].floatValue < 7.0){
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            
            CGContextTranslateCTM(bitmap, pickedImage.frame.size.width -28, pickedImage.frame.size.height -28);
        }
        else
        {
            CGContextTranslateCTM(bitmap, pickedImage.frame.size.width, pickedImage.frame.size.height);
        }
    }else{
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            
            CGContextTranslateCTM(bitmap, pickedImage.frame.size.width, pickedImage.frame.size.height);
        }
        else
        {
            CGContextTranslateCTM(bitmap, pickedImage.frame.size.width -29, pickedImage.frame.size.height -29);
        }
    }
    
    CGContextRotateCTM(bitmap, rotateAngle);
    CGContextScaleCTM(bitmap, 1.0f, -1.0f);
    CGContextDrawImage(bitmap, CGRectMake(-img.size.width / 2.0f,
                                          -img.size.height / 2.0f,
                                          img.size.width,
                                          img.size.height),
                       img.CGImage);
    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    img = resultImage;
}

- (void)loadView{
    
    if([[UIScreen mainScreen] bounds].size.height == 568)
    {
        // iPhone 5
        self.view = [[NSBundle mainBundle] loadNibNamed:@"PhotoLibrary" owner:self options:nil][0];
    }
    else
    {
        self.view = [[NSBundle mainBundle] loadNibNamed:@"PhotoLibraryFour" owner:self options:nil][0];
    }
    
}

- (void)viewDidAppear:(BOOL)animated{
    imageRotateCounter = 1;
    [self.view addSubview:slider];
    adjustTran.hidden = TRUE;
    firstPhoto = [[NSUserDefaults standardUserDefaults] stringForKey:@"base_photo"];
    if([firstPhoto isEqualToString:@"firstUpload"]){
        slider.hidden = TRUE;
        transparent.hidden = TRUE;
        opaque.hidden = TRUE;
        [self goToPhotoLibrary];
    }else if ([firstPhoto isEqualToString:@"upload"]){
        [[NSUserDefaults standardUserDefaults] setObject:@"third" forKey:@"base_photo"];
        slider.hidden = FALSE;
        transparent.hidden = FALSE;
        opaque.hidden = FALSE;
        [self goToPhotoLibrary];
    }else if ([firstPhoto isEqualToString:@"firstUploaded"]){
        [[NSUserDefaults standardUserDefaults] setObject:@"second" forKey:@"base_photo"];
        slider.hidden = TRUE;
        transparent.hidden = TRUE;
        opaque.hidden = TRUE;
    }else if ([firstPhoto isEqualToString:@"uploaded"]){
        NSString *flag = [[NSUserDefaults standardUserDefaults] stringForKey:@"not_first_run"];
        if(!flag){
            adjustTran.hidden = FALSE;
        }else{
            adjustTran.hidden = TRUE;
        }
        slider.hidden = FALSE;
        transparent.hidden = FALSE;
        opaque.hidden = FALSE;
    }else if ([firstPhoto isEqualToString:@"first"]){
        [self dismissViewControllerAnimated:NO completion:NULL];
        slider.hidden = TRUE;
        transparent.hidden = TRUE;
        opaque.hidden = TRUE;
    }
    else if ([firstPhoto isEqualToString:@"cancelUpload"]){
        [self dismissViewControllerAnimated:NO completion:NULL];
        slider.hidden = FALSE;
        transparent.hidden = FALSE;
        opaque.hidden = FALSE;
    }
}

- (void)viewDidLoad
{
        
    [blendedImageView setImage:blendedImage];
    [blendedImageView setAlpha:0.5];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
