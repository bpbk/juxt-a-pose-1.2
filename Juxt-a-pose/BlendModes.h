//
//  BlendModes.h
//  Juxt-a-pose
//
//  Created by Brandon Phillips on 8/21/13.
//  Copyright (c) 2013 We Are Station. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlendModes : UIViewController<UIScrollViewDelegate>{

    IBOutlet UIView *blendView;
    IBOutlet UIScrollView *blendScroll;
    IBOutlet UIImageView *topView;
    IBOutlet UIImageView *bottomView;
    IBOutlet UIImageView *combinedView;
}


- (IBAction)blendMultiply;

@end
