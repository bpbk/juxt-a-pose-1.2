//
//  PhotoLibrary.h
//  Juxt-a-pose
//
//  Created by Brandon Phillips on 7/3/13.
//  Copyright (c) 2013 We Are Station. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoLibrary : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>{
    UIImagePickerController *picker2;
    UIImage *image;
    IBOutlet UIImageView *pickedImage;
    IBOutlet UIImageView *blendedImageView;
    NSString *photoLibraryStatus;
    CGRect bounds;
    UIImage *imageCopy;
    IBOutlet UISlider *slider;
    IBOutlet UIImageView *opaque;
    IBOutlet UIImageView *transparent;
    NSString *firstPhoto;
    UIImageView *topView;
    IBOutlet UIImageView *adjustTran;
    int imageRotateCounter;
    float rotateAngle;
}

- (IBAction)usePhoto;
- (IBAction)sliderChanged:(id)sender;
- (IBAction)cancel;
- (IBAction)rotatePickedImage;

@end
