//
//  BlendModes.m
//  Juxt-a-pose
//
//  Created by Brandon Phillips on 8/21/13.
//  Copyright (c) 2013 We Are Station. All rights reserved.
//

#import "BlendModes.h"

@interface BlendModes ()

@end

@implementation BlendModes

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (IBAction)blendMultiply{
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configureScrollView];
    // Do any additional setup after loading the view from its nib.
}
-(void)topImage: (UIImage *)topImageLayer bottomImage: (UIImage *)bottomImageLayer sliderFloat: (float)sliderValue{
     [bottomView setImage:bottomImageLayer];
     [topView setImage:topImageLayer];
     [topView setAlpha:1.0 - sliderValue];
     [bottomView addSubview:topView];
}

- (void)configureScrollView {
    CGSize size = blendView.bounds.size;
    blendView.frame = CGRectMake(0, 0, size.width, size.height);
    [blendScroll addSubview:blendView];
    blendScroll.contentSize = size;
    blendScroll.delegate = self;
    // If you don't use self.contentView anywhere else, clear it here.
    blendView = nil;
    // If you use it elsewhere, clear it in `dealloc` and `viewDidUnload`.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
