//
//  JPic.h
//  Juxt-a-pose
//
//  Created by Christopher on 11/19/13.
//  Copyright (c) 2013 We Are Station. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JPic : NSObject{

    
}

@property (nonatomic,retain)UIImage *image;
@property (nonatomic)CGFloat opacity;
@property (nonatomic)CGPoint position;
@property (nonatomic)CGPoint size;


- (id)initWithOptions:(UIImage*)images
                   op:(CGFloat)opacitys
                  pos:(CGPoint)positions
                  siz:(CGPoint)sizes;




@end
