//
//  ViewController.h
//  Juxt-a-pose
//
//  Created by Brandon Phillips on 5/25/13.
//  Copyright (c) 2013 We Are Station. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <QuartzCore/QuartzCore.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <MediaPlayer/MediaPlayer.h>
#import "EditPreviewView.h"

@class JxtaPic, JPic;


@interface ViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate, UIScrollViewDelegate>{

    
    /*-------Juxtapose Objects-------*/
    JxtaPic *newJuxtaposeImage;
    
    /*-------PREVIEW IMAGE------*/
    
    CGFloat lastScale;
    IBOutlet UIView *coverPreview;
    UIImageView *coverBg;
    float senderScale;
    NSTimer *shootTimer;
    
    /*---------TUTORIAL---------*/
    
    IBOutlet UIButton *addAnother;
    UIImagePickerController *picker;
    IBOutlet UIImageView *saveIt;
    int clickTimes;
    IBOutlet UIView *clickBlink;
    int clickShootTimes;
    IBOutlet UIView *clickShootBlink;
    IBOutlet UIImageView *chooseBlend;
    IBOutlet UIImageView *saveBlended;
    
    /*----------CAMERA----------*/
    
    UIImageView *bigGreen;
    UIImageView *bigOrange;
    //UINavigationController *cameraNav;
    NSData *pngData;
    UIImage * libraryImage;
    IBOutlet UIImageView* overlayView;
    UIImageView * newImage;
    UIImageView* topView;
    IBOutlet UISlider *slider;
    NSString *firstPhoto;
    //UIImage *img;
    //UIImageView *rotatedView;
    UIView *realOverlayView;
    UIImage *takenPhoto;
    CGFloat overlayAlpha;
    CGRect bounds;
    NSString *flashStatus;
    int cameraRotation;
    IBOutlet UIImageView *transparent;
    IBOutlet UIImageView *opaque;
    IBOutlet UIView *saveCover;
    IBOutlet UIView *camBG;
    IBOutlet UIView *previewView;
    IBOutlet UIImageView *previewImage;
    IBOutlet UIImageView *originalPreviewImage;
    IBOutlet UIView *takePicCover;
    
    __weak IBOutlet UIButton *toggleFlashMode;
    UIImage *imageCopy;
    int cameraSource;
    NSString *uploadPhoto;
    IBOutlet UIButton *tutShoot;
    IBOutlet UIImageView *adjustTran;
    int imageRotateCounter;
    float rotateAngle;
    float timer;
    
    IBOutlet UIImageView *combinedView;
    UIImageView* blendTheBottomView;
    UIImageView *blendTheTopView;
    
    /*---------MAIN VIEW--------*/
    
   // UIButton *seeAll;
    
    UIImage *image;
    
    IBOutlet UIImageView *imageView;
    IBOutlet UIImageView *another;
    IBOutlet UIView *blendView;
    IBOutlet UIView *blendScrollView;
    IBOutlet UIScrollView *blendScroll;
    IBOutlet UIImageView *topBlendView;
    IBOutlet UIImageView *bottomBlendView;
    //IBOutlet UIImageView *combinedView;
    IBOutlet UIView *blendModeBox;
    
    /*-------------MANIPULATE-------------*/
    
    //UIPinchGestureRecognizer *pinch;
    //UIRotationGestureRecognizer *rotation;
    //UIPanGestureRecognizer *pan;
    
   
    
    CGFloat _lastScale;
    CGFloat _lastRotation;
    CGFloat _firstX;
    CGFloat _firstY;
    CGPoint movePoint;
    
    CGFloat AcuScale;
    CGFloat AcuRotation;
    CGFloat PosX;
    CGFloat PosY;
    
    bool rotating;
    bool scaling;
    bool moving;
    
    CGFloat frameX;
    CGFloat frameY;
    
    /*-----------BLEND BUTTONS------------*/
    
    IBOutlet UIButton *blendColorDodge;
    IBOutlet UIButton *blendLuminosity;
    IBOutlet UIButton *blendBackToNormal;
    IBOutlet UIButton *blendMultiply;
    IBOutlet UIButton *blendColorBurn;
    IBOutlet UIButton *blendDifference;
    IBOutlet UIButton *blendExclusion;
    IBOutlet UIButton *blendHardlight;
    IBOutlet UIButton *blendOverlay;
    IBOutlet UIButton *blendScreen;
    IBOutlet UIButton *blendSaturation;
    IBOutlet UIButton *blendHue;
    IBOutlet UIButton *blendColor;
    IBOutlet UIButton *blendDarker;
    IBOutlet UIButton *blendLighter;
    IBOutlet UIButton *blendSoftLight;
    IBOutlet UIView *buttonUnderline;
    IBOutlet UIView *arrow;
    
    /*-----------------VIDEO--------------*/
    
    IBOutlet UIView *videoView;
    NSMutableArray *videoFrames;
    float time;
    float videoTimeAmount;
    NSTimer *videoTimer;
    
}

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) AVAudioRecorder *audioRecorder;


-(IBAction)ifClicked;
-(IBAction)ifShootClicked;
-(IBAction)home;
-(IBAction)cancelPhoto;
-(IBAction)cameraFlip;
-(IBAction)finished;
-(IBAction)usePhoto;
-(IBAction)seeAll;
-(IBAction)TakePhoto;
-(IBAction)FlashToggle;
-(IBAction)ChooseExisting;
//-(IBAction)photosPage;
-(IBAction)sliderChanged:(id)sender;
-(IBAction)shoot;
-(IBAction)cancelPreview;
-(IBAction)rotateTakenImage;
//- (IBAction)rotateGesture:(UIRotationGestureRecognizer *)sender;


//- (IBAction)pinchGesture:(UIPinchGestureRecognizer *)sender;
//- (IBAction)panGesture:(UIPanGestureRecognizer *)sender;
- (IBAction)setupBlendScreen;
- (IBAction)blendColorDodge;
- (IBAction)blendLuminosity;
- (IBAction)blendDarken;
- (IBAction)blendIt:(id)sender;
- (IBAction)blendBackToNormal;
- (IBAction)blendMultiply;
- (IBAction)blendColorBurn;
- (IBAction)blendDifference;
- (IBAction)blendExclusion;
- (IBAction)blendHardlight;
- (IBAction)blendOverlay;
- (IBAction)blendScreen;
- (IBAction)blendSaturation;
- (IBAction)blendHue;
- (IBAction)blendColor;
- (IBAction)blendDarker;
- (IBAction)blendLighter;
- (IBAction)blendLighten;
- (IBAction)blendSoftLight;
- (IBAction)cancelBlendMode;

/*- (IBAction)videoButton;
- (IBAction)startVideo;
- (IBAction)stopVideo;
- (IBAction)backToImageCapture; */

//@property (nonatomic, retain) IBOutlet UILabel *sliderValue;
@property (nonatomic) BOOL showsCameraControls;

//- (UIImage *)fixOrientation;
@end
